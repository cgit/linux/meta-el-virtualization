DESCRIPTION = "Image for the host side of the Enea Linux Virtualization profile"

require images/enea-image-common.inc
require images/enea-image-virtualization-common.inc

IMAGE_INSTALL += " \
    packagegroup-enea-virtualization-host \
    packagegroup-enea-virtualization-tools \
    "

IMAGE_ROOTFS_EXTRA_SPACE = "131072"
IMAGE_OVERHEAD_FACTOR = "2"
